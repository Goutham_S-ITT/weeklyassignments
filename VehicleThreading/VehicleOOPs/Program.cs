﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace VehicleThreading
{
    class Program
    {
        static int vehicleChoice = 0;
        public List<Car> Cars = new List<Car>();

        /// <summary>
        /// For selecting vehicle
        /// </summary>
        public void SelectionOFVehicle()
        {
            Console.WriteLine("\n\n\t\t\t\tWelcome To Tesla\n");
            Console.WriteLine("\n Select vehicle \n\n 1. Cars \n");
            Console.Write("\n Enter your operationChoice : ");
            try
            {
                bool result = int.TryParse(Console.ReadLine(), out vehicleChoice);
                if (result & vehicleChoice > 0 & vehicleChoice < 2)
                {
                    SelectionOfOperation();
                }
            }
            catch
            {
                Console.WriteLine("\n Provide input as 1 \n");
            }
        }
        
        /// <summary>
        /// For selecting operation for selected vehicle
        /// </summary>
        public void SelectionOfOperation()
        {
            Car car = new Car();
            Console.WriteLine("\n Select operation to be performed \n\n 1. Add \n\n 2. Find \n\n 3. Update \n\n 4. Delete \n\n 5. List \n\n 6. Exit \n");
            Console.Write("\n Enter your operationChoice : ");
            try
            {
                int operationChoice;
                bool result = int.TryParse(Console.ReadLine(), out operationChoice);
                if (result & operationChoice > 0 & operationChoice < 7)
                {
                    switch (vehicleChoice)
                    {
                        case 1:
                            switch (operationChoice)
                            {
                                case 1:
                                    Thread addThread = new Thread(car.AddVehicle);
                                    Console.WriteLine("\n Main thread hand over add operation to add thread\n");
                                    addThread.Start();
                                    //car.AddVehicle();
                                    addThread.Join();
                                    //while(addThread.IsAlive)
                                    Console.WriteLine("\n Main thread has taken over the execution\n");
                                    Cars.Add(car);
                                    break;
                                case 2:
                                    ParameterizedThreadStart parameterizedFindThread = new ParameterizedThreadStart(car.FindVehicle);
                                    Thread findThread = new Thread(parameterizedFindThread);
                                    Console.WriteLine("\n Main thread hand over find operation to find thread\n");
                                    findThread.Start(Cars);
                                    findThread.Join();
                                    Console.WriteLine("\n Main thread has taken over the execution\n");
                                    //car.FindVehicle(Cars);
                                    break;
                                case 3:
                                    ParameterizedThreadStart parameterizedUpdateThread = new ParameterizedThreadStart(car.UpdateVehicle);
                                    Thread updateThread = new Thread(parameterizedUpdateThread);
                                    Console.WriteLine("\n Main thread hand over update operation to update thread\n");
                                    updateThread.Start(Cars);
                                    updateThread.Join();
                                    Console.WriteLine("\n Main thread has taken over the execution\n");
                                    //car.UpdateVehicle(Cars);
                                    break;
                                case 4:
                                    ParameterizedThreadStart parameterizedDeleteThread = new ParameterizedThreadStart(car.DeleteVehicle);
                                    Thread deleteThread = new Thread(parameterizedDeleteThread);
                                    Console.WriteLine("\n Main thread hand over delete operation to delete thread\n");
                                    deleteThread.Start(Cars);
                                    deleteThread.Join();
                                    Console.WriteLine("\n Main thread has taken over the execution\n");
                                    //car.DeleteVehicle(Cars);
                                    break;
                                case 5:
                                    ParameterizedThreadStart parameterizedListThread = new ParameterizedThreadStart(car.ListVehicle);
                                    Thread listThread = new Thread(parameterizedListThread);
                                    Console.WriteLine("\n Main thread hand over list operation to list thread\n");
                                    listThread.Start(Cars);
                                    listThread.Join();
                                    Console.WriteLine("\n Main thread has taken over the execution\n");
                                    //car.ListVehicle(Cars);
                                    break;
                                case 6:
                                    SelectionOFVehicle();
                                    break;
                                default:
                                    Console.WriteLine("\n Provide input from 1 to 6 \n");
                                    SelectionOfOperation();
                                    break;
                            }
                            break;
                        default:
                            Console.WriteLine("\n Provide input as 1 \n");
                            break;
                    }
                }
            }
            catch
            {
                Console.WriteLine("\n Invalid input please try again \n");
            }
        }

        static void Main()
        {
            Program vehicle = new Program();
            while (true)
            {
                vehicle.SelectionOFVehicle();
            }
        }
    }
}

