﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace VehicleThreading
{
    public class Car : Vehicle, IOperationOnVehicle
    {
        public int Airbags { get; set; }

        /// <summary>
        /// For adding new vehicle
        /// </summary>
        public override void AddVehicle()
        {
            Console.WriteLine("\n Child thread add has started to execute\n");
            int data = 0;
            Console.Write("\n Enter number of airbags : ");
            try
            {
                bool result = int.TryParse(Console.ReadLine(), out data);
                if (result)
                {
                    Airbags = data;
                    base.AddVehicle();
                }
                else
                {
                    Console.WriteLine("\n Invalid input please try again \n");
                    AddVehicle();
                }
                Console.WriteLine("\n Child thread add has finished its execution\n");
            }
            catch
            {
                Console.WriteLine("\n Provide input in numbers \n");
            }
        }

        /// <summary>
        /// For finding vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void FindVehicle(object cars)
        { 
            lock (cars)
            {
                Console.WriteLine("\n Child thread find has started to execute\n");
                List<Car> carList = (List<Car>)cars;
                Console.Write("\n Enter engine number : ");
                string EngineNumber = Console.ReadLine();
                try
                {
                    if (carList.Exists(cond => cond.EngineNumber == EngineNumber))
                    {
                        Car car = carList.Find(cond => cond.EngineNumber == EngineNumber);
                        Console.WriteLine("\n Engine number = {0}, Colour = {1} ,Type of fuel = {2}", car.EngineNumber, car.VehicleColour, car.TypeOfFuel);
                        Console.WriteLine(" Number of airbags = {0}", car.Airbags);
                    }
                    else
                    {
                        Console.WriteLine("\n No cars found \n");
                    }
                    Console.WriteLine("\n Child thread find has finished its execution\n");
                }
                catch
                {
                    Console.WriteLine("\n Engine number provided does not exists \n");
                }
            }
        }

        /// <summary>
        /// For updating details of vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void UpdateVehicle(object cars)
        {
            lock (cars)
            {
                Console.WriteLine("\n Child thread update has started to execute\n");
                List<Car> carList = (List<Car>)cars;
                Console.Write("\n Enter engine number : ");
                string EngineNumber = Console.ReadLine();
                try
                {
                    if (carList.Exists(cond => cond.EngineNumber == EngineNumber))
                    {
                        Car car = carList.Find(cond => cond.EngineNumber == EngineNumber);
                        Console.WriteLine("\n Select operation to be performed \n\n 1. Update engine number \n\n 2. Update vehicle colour \n\n 3. Update type of fuel \n\n 4. Update number of airbags \n\n 5. Exit \n");
                        Console.Write("\n Enter your choice : ");
                        int data;
                        bool result = int.TryParse(Console.ReadLine(), out data);
                        if (result)
                        {
                            switch (data)
                            {
                                case 1:
                                    Console.Write("\n Enter the engine number : ");
                                    car.EngineNumber = Console.ReadLine();
                                    break;
                                case 2:
                                    Console.Write("\n Enter the vehicle colour : ");
                                    car.VehicleColour = Console.ReadLine();
                                    break;
                                case 3:
                                    Console.Write("\n Enter type of fuel : ");
                                    car.TypeOfFuel = Console.ReadLine();
                                    break;
                                case 4:
                                    Console.Write("\n Enter number of Airbags : ");
                                    result = int.TryParse(Console.ReadLine(), out data);
                                    if (result)
                                    {
                                        car.Airbags = data;
                                    }
                                    else
                                    {
                                        Console.WriteLine("\n Invalid input please try again \n");
                                    }
                                    break;
                                case 5:
                                    break;
                                default:
                                    Console.WriteLine("\n Invalid input please try again \n");
                                    break;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("\n No car found \n");
                    }
                    Console.WriteLine("\n Child thread update has finished its execution\n");
                }
                catch
                {
                    Console.WriteLine("\n Engine number does not exists \n");
                }
            }
        }

        /// <summary>
        /// For Deleting a vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void DeleteVehicle(object cars)
        {
            lock (cars)
            {
                Console.WriteLine("\n Child thread delete has started to execute\n");
                List<Car> carList = (List<Car>)cars;
                Console.Write("\n Enter engine number : ");
                string EngineNumber = Console.ReadLine();
                try
                {
                    if (carList.Exists(cond => cond.EngineNumber == EngineNumber))
                    {
                        Car car = carList.Find(cond => cond.EngineNumber == EngineNumber);
                        carList.Remove(car);
                        Console.WriteLine("\n Data deleted successfully \n");
                    }
                    else
                    {
                        Console.WriteLine("\n No car found \n");
                    }
                    Console.WriteLine("\n Child thread delete has finished its execution\n");
                }
                catch
                {
                    Console.WriteLine("\n Engine number does not exists \n");
                }
            }
        }

        /// <summary>
        /// For listing all vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void ListVehicle(object cars)
        {
            lock (cars)
            {
                Console.WriteLine("\n Child thread list has started to execute\n");
                List<Car> carList = (List<Car>)cars;
                try
                {
                    if (carList.Count != 0)
                    {
                        foreach (Car Car in carList)
                        {
                            Console.WriteLine("\n Engine number = {0}, Colour = {1} ,Type of fuel = {2}", Car.EngineNumber, Car.VehicleColour, Car.TypeOfFuel);
                            Console.WriteLine(" Number of airbags = {0}", Car.Airbags);
                        }
                    }
                    else
                    {
                        Console.WriteLine("\n No Cars available \n");
                    }
                    Console.WriteLine("\n Child thread list has finished its execution\n");
                }
                catch
                {
                    Console.WriteLine("\n No cars found \n");
                }
            }
        }
    }
}
