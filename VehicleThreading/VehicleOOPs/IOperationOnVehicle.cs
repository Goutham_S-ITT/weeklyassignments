﻿using System.Collections.Generic;

namespace VehicleThreading
{
    public interface IOperationOnVehicle
    {
        /// <summary>
        /// For adding new vehicle 
        /// </summary>
        void AddVehicle();

        /// <summary>
        /// For finding vehicle
        /// </summary>
        /// <param name="cars"></param>
        void FindVehicle(object cars);

        /// <summary>
        /// For Updating details of vehicle
        /// </summary>
        /// <param name="cars"></param>
        void UpdateVehicle(object cars);

        /// <summary>
        /// For deleting a vehicle
        /// </summary>
        /// <param name="cars"></param>
        void DeleteVehicle(object cars);

        /// <summary>
        /// For list all vehicle
        /// </summary>
        /// <param name="cars"></param>
        void ListVehicle(object cars);
    }
}
