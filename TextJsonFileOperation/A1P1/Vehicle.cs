﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace Vehicle
{
    public class Vehicle
    {
        public string EngineNumber { get; set; }
        public string VehicleColour { get; set; }
        public string TypeOfFuel { get; set; }

        /// <summary>
        /// For adding new vehicle
        /// </summary>
        public virtual void AddVehicle()
        {
            Console.Write("\n Enter engine number : ");
            EngineNumber = Console.ReadLine();
            Console.Write("\n Enter vehicle colour : ");
            VehicleColour = Console.ReadLine();
            Console.Write("\n Enter type of fuel : ");
            TypeOfFuel = Console.ReadLine();
        }

        /// <summary>
        /// To perform read/write operation using text file
        /// </summary>
        /// <param name="cars"></param>
        public void TextFileOperation(List<Car> cars)
        {
            string dataRead;
            string[] dataEntries;
            string filePathCarDataText = "./CarData.txt";
            Console.WriteLine("\n 1. Write \n\n 2. Read \n");
            Console.Write("\n Enter your choice : ");
            int fileChoice = 0;
            bool result = int.TryParse(Console.ReadLine(), out fileChoice);
            try
            {
                if (result)
                {
                    switch (fileChoice)
                    {
                        case 1:
                            StreamWriter writer = new StreamWriter(filePathCarDataText);
                            if (cars.Count != 0)
                            {
                                foreach (Car car in cars)
                                {
                                    writer.Write(car.EngineNumber);
                                    writer.Write(" , ");
                                    writer.Write(car.TypeOfFuel);
                                    writer.Write(" , ");
                                    writer.Write(car.VehicleColour);
                                    writer.Write(" , ");
                                    writer.Write(car.Airbags + " , ");
                                }
                                Console.WriteLine("\n Data write is successfull \n");
                            }
                            else
                            {
                                Console.WriteLine("\n No data to write \n");
                            }
                            writer.Close();
                            break;
                        case 2:
                            StreamReader reader = new StreamReader(filePathCarDataText);
                            dataRead = reader.ReadToEnd();
                            if (dataRead != null)
                            {
                                dataEntries = dataRead.Split(',');
                                for (int index = 0; index < dataEntries.Length - 1;)
                                {
                                    Car newcar = new Car();
                                    newcar.EngineNumber = dataEntries[index++];
                                    newcar.TypeOfFuel = dataEntries[index++];
                                    newcar.VehicleColour = dataEntries[index++];
                                    newcar.Airbags = int.Parse(dataEntries[index++]);
                                    cars.Add(newcar);
                                }
                                Console.WriteLine("\n Data read is successfull \n");
                            }
                            else
                            {
                                Console.WriteLine("\n No data to read \n");
                            }
                            reader.Close();
                            break;
                        default:
                            Console.WriteLine("\n Invalid input please try again \n");
                            break;
                    }
                }
            }
            catch
            {
                Console.WriteLine("/n Unable to access text file /n");
            }
        }

        /// <summary>
        /// To perform read/write operation using json file
        /// </summary>
        /// <param name="cars"></param>
        public void JsonFileOperation(List<Car> cars)
        {
            string dataRead;
            string filePathCarDataJson = "./CarData.json";
            Console.WriteLine("\n 1. Write \n\n 2. Read \n");
            Console.Write("\n Enter your choice : ");
            int fileChoice = 0;
            bool result = int.TryParse(Console.ReadLine(), out fileChoice);
            try
            {
                if (result)
                {
                    switch (fileChoice)
                    {
                        case 1:
                            StreamWriter writer = new StreamWriter(filePathCarDataJson);
                            if (cars.Count != 0)
                            {
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                string jsonData = js.Serialize(cars);
                                writer.Write(jsonData);
                                Console.WriteLine("\n Data write is successfull \n");
                            }
                            else
                            {
                                Console.WriteLine("\n No data to write \n");
                            }
                            writer.Close();
                            break;
                        case 2:
                            StreamReader reader = new StreamReader(filePathCarDataJson);
                            dataRead = reader.ReadToEnd();
                            if (dataRead != null)
                            {
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                cars.AddRange(js.Deserialize<List<Car>>(dataRead));
                                Console.WriteLine("\n Data read is successfull \n");
                            }
                            else
                            {
                                Console.WriteLine("\n No data to read \n");
                            }
                            reader.Close();
                            break;
                        default:
                            Console.WriteLine("\n Invalid input please try again \n");
                            break;
                    }
                }
            }
            catch
            {
                Console.WriteLine("/n Unable to access json file /n");
            }
        }
    }
}
