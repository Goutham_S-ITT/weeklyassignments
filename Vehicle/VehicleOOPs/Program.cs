﻿using System;
using System.Collections.Generic;

namespace Vehicle
{
    class Program
    {
        static int vehicleChoice = 0;
        public List<Car> Cars = new List<Car>();

        /// <summary>
        /// For selecting vehicle
        /// </summary>
        public void SelectionOFVehicle()
        {
            Console.WriteLine("\n\n\t\t\t\tWelcome To Tesla\n");
            Console.WriteLine("\n Select vehicle \n\n 1. Cars \n");
            Console.Write("\n Enter your operationChoice : ");
            try
            {
                bool result = int.TryParse(Console.ReadLine(), out vehicleChoice);
                if (result & vehicleChoice > 0 & vehicleChoice < 2)
                {
                    SelectionOfOperation();
                }
            }
            catch
            {
                Console.WriteLine("\n Provide input as 1 \n");
            }
        }

        /// <summary>
        /// For selecting operation for selected vehicle
        /// </summary>
        public void SelectionOfOperation()
        {
            Car car = new Car();
            Console.WriteLine("\n Select operation to be performed \n\n 1. Add \n\n 2. Find \n\n 3. Update \n\n 4. Delete \n\n 5. List \n\n 6. Exit \n");
            Console.Write("\n Enter your operationChoice : ");
            try
            {
                int operationChoice;
                bool result = int.TryParse(Console.ReadLine(), out operationChoice);
                if (result & operationChoice > 0 & operationChoice < 7)
                {
                    switch (vehicleChoice)
                    {
                        case 1:
                            switch (operationChoice)
                            {
                                case 1:
                                    car.AddVehicle();
                                    Cars.Add(car);
                                    break;
                                case 2:
                                    car.FindVehicle(Cars);
                                    break;
                                case 3:
                                    car.UpdateVehicle(Cars);
                                    break;
                                case 4:
                                    car.DeleteVehicle(Cars);
                                    break;
                                case 5:
                                    car.ListVehicle(Cars);
                                    break;
                                case 6:
                                    SelectionOFVehicle();
                                    break;
                                default:
                                    Console.WriteLine("\n Provide input from 1 to 6 \n");
                                    SelectionOfOperation();
                                    break;
                            }
                            break;
                        default:
                            Console.WriteLine("\n Provide input as 1 \n");
                            break;
                    }
                }
            }
            catch
            {
                Console.WriteLine("\n Invalid input please try again \n");
            }
        }

        static void Main()
        {
            Program vehicle = new Program();
            while (true)
            {
                vehicle.SelectionOFVehicle();
            }
        }
    }
}

