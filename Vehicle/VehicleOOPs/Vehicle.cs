﻿using System;

namespace Vehicle
{
    public class Vehicle
    {
        public string EngineNumber { get; set; }
        public string VehicleColour { get; set; }
        public string TypeOfFuel { get; set; }

        /// <summary>
        /// For adding new vehicle
        /// </summary>
        public virtual void AddVehicle()
        {
            Console.Write("\n Enter engine number : ");
            EngineNumber = Console.ReadLine();
            Console.Write("\n Enter vehicle colour : ");
            VehicleColour = Console.ReadLine();
            Console.Write("\n Enter type of fuel : ");
            TypeOfFuel = Console.ReadLine();
        }
    }
}
