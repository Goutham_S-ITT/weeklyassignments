﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CompanyDetailsWebApi.Models
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string Grade { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public int DepartmentId { get; set; }

        public DepartmentViewModel Department { get; set; }
        public SalaryViewModel Salary { get; set; }
    }
}