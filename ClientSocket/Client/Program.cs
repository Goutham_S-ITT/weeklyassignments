﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    class Program
    {
        static string text;
        static string data;
        static byte[] buffer { get; set; }
        static Socket socket;
        static Thread sendThread = new Thread(send);
        static Thread recieveThread = new Thread(recieve);
        static void Main()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1111);
            
            try
            {
                socket.Connect(localEndPoint);
            }
            catch
            {
                Console.WriteLine("\n Unable to connect to the sever \n"); 
            }
            Console.WriteLine("\n Connected to the server \n");
            sendThread.Start();
            recieveThread.Start();
            recieveThread.Join();
            //sendThread.Abort();
            socket.Close();
        }

        public static void send()
        {
            while (true)
            {
                //Console.Write("\n Enter text : ");
                text = Console.ReadLine();
                Console.WriteLine("\n << {0} ", text);
                byte[] data = Encoding.ASCII.GetBytes(text);
                socket.Send(data);
                //Console.WriteLine("\n Data send successfully \n");
            }
        }

        public static void recieve()
        {
            while(true)
            {
                buffer = new byte[socket.ReceiveBufferSize];
                int bytesRead = socket.Receive(buffer);
                byte[] formatted = new byte[bytesRead];
                for (int i = 0; i < bytesRead; i++)
                {
                    formatted[i] = buffer[i];
                }
                data = Encoding.ASCII.GetString(formatted);
                Console.WriteLine("\n >> {0}", data);
            } 
        }
    }
}
