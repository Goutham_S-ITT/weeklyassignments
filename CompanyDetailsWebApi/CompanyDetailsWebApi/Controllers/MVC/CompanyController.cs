﻿using CompanyDetailsWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace CompanyDetailsWebApi.Controllers.MVC
{
    public class CompanyController : Controller
    {
        CompanyDatabaseContext CompanyDatabase = new CompanyDatabaseContext();

        /// <summary>
        /// To list all employees
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            IEnumerable<Employee> employees = null;
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:61703/api/");
                var responseTask = client.GetAsync("Company");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Employee>>();
                    readTask.Wait();
                    employees = readTask.Result;
                }
                else
                {
                    employees = Enumerable.Empty<Employee>();
                }
                return View(employees);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Please contact administrator.");
                return View(employees);
            }
        }

        /// <summary>
        /// To display employee create page
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            BindDepartmentId();
            BindDepartmentGrade();
            return View();
        }

        /// <summary>
        /// To create employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Create")]
        public ActionResult Create(Employee employee)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:61703/api/");
                var postTask = client.PostAsJsonAsync<Employee>("Company", employee);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return View(employee);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Please contact administrator.");
                return View(employee);
            }
        }

        /// <summary>
        /// To display employee edit page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            BindDepartmentId();
            BindDepartmentGrade();
            Employee employee = null;
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:61703/api/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var responseTask = client.GetAsync("Company?id=" + id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Employee[]>();
                    readTask.Wait();
                    employee = readTask.Result.FirstOrDefault();
                }
                return View(employee);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Please contact administrator.");
                return View(employee);
            }
        }

        /// <summary>
        /// To Edit Employee details
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Edit")]
        public ActionResult Edit(Employee employee, int id)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:61703/api/");
                var putTask = client.PutAsJsonAsync<Employee>("Company?id=" + id.ToString(), employee);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return View(employee);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Please contact administrator.");
                return View(employee);
            }
        }

        /// <summary>
        /// To delete employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:61703/api/");
                var deleteTask = client.DeleteAsync("Company/" + id.ToString());
                deleteTask.Wait();
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Please contact administrator.");
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// To display employees under department
        /// </summary>
        /// <returns></returns>
        public ActionResult Departments(int id)
        {
            IEnumerable<Employee> employees = null;
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:61703/api/");
                var responseTask = client.GetAsync("Company");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Employee>>();
                    readTask.Wait();
                    if (id == 1)
                    {
                        ViewData["Department"] = "Marketing Dept";
                        employees = readTask.Result.Where(cond => cond.Department.DepartmentName == "Marketing");
                    }
                    if (id == 2)
                    {
                        ViewData["Department"] = "Sales Dept";
                        employees = readTask.Result.Where(cond => cond.Department.DepartmentName == "Sales");
                    }
                    if (id == 3)
                    {
                        ViewData["Department"] = "HR Dept";
                        employees = readTask.Result.Where(cond => cond.Department.DepartmentName == "HR");
                    }
                }
                else
                {
                    employees = Enumerable.Empty<Employee>();

                }
                return View(employees);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Please contact administrator.");
                return View(employees);
            }
        }

        /// <summary>
        /// To get departmentId data for having drop list
        /// </summary>
        public void BindDepartmentId()
        {
            List<SelectListItem> departmentId = new List<SelectListItem>();
            List<Department> departmentList = CompanyDatabase.Departments.ToList();
            foreach (Department item in departmentList)
            {
                departmentId.Add(new SelectListItem { Text = item.DepartmentName, Value = item.DepartmentId.ToString() });
            }
            ViewData["DepartmentId"] = departmentId;
        }

        /// <summary>
        /// To get grade data for having drop list
        /// </summary>
        public void BindDepartmentGrade()
        {
            List<SelectListItem> grade = new List<SelectListItem>();
            List<Salary> salaryList = CompanyDatabase.Salaries.ToList();
            foreach (Salary item in salaryList)
            {
                grade.Add(new SelectListItem { Text = item.Grade, Value = item.Grade });
            }
            ViewData["Grade"] = grade;
        }
    }
}
