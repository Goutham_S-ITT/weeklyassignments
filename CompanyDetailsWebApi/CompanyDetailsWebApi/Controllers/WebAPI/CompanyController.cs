﻿using CompanyDetailsWebApi.Models;
using System;
using System.Collections;
using System.Linq;
using System.Web.Http;

namespace CompanyDetailsWebApi.Controllers
{
    public class CompanyController : ApiController
    {
        CompanyDatabaseContext CompanyDatabase = new CompanyDatabaseContext();

        /// <summary>
        /// To list all employees
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult GetAllEmployees()
        {
            var employees = CompanyDatabase.Employees
                        .Select(s => new EmployeeViewModel()
                        {
                            EmployeeId = s.EmployeeId,
                            Name = s.Name,
                            Email = s.Email,
                            Gender = s.Gender,
                            Grade = s.Grade,
                            DepartmentId = s.DepartmentId,
                            Department = new DepartmentViewModel()
                            {
                                DepartmentName = s.Department.DepartmentName
                            },
                            Salary = new SalaryViewModel()
                            {
                                SalaryRange = s.Salary.SalaryRange
                            }
                        }).ToList<EmployeeViewModel>();
            if (employees.Count == 0)
            {
                return NotFound();
            }
            return Ok(employees.AsEnumerable());
        }

        /// <summary>
        /// To find particular employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IHttpActionResult GetEmployee(int id)
        {
            var employee = CompanyDatabase.Employees.Where(cond => cond.EmployeeId == id)
                        .Select(s => new EmployeeViewModel()
                        {
                            EmployeeId = s.EmployeeId,
                            Name = s.Name,
                            Email = s.Email,
                            Gender = s.Gender,
                            Grade = s.Grade,
                            DepartmentId = s.DepartmentId,
                            Department = new DepartmentViewModel()
                            {
                                DepartmentName = s.Department.DepartmentName
                            },
                            Salary = new SalaryViewModel()
                            {
                                SalaryRange = s.Salary.SalaryRange
                            }
                        });
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee.AsEnumerable());
        }

        /// <summary>
        /// To add new employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public IHttpActionResult PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }
            CompanyDatabase.Employees.Add(employee);
            CompanyDatabase.SaveChanges();
            return Ok("Employee added successfully");
        }

        /// <summary>
        /// To update details of an employee
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateEmployee"></param>
        /// <returns></returns>
        public IHttpActionResult PutEmployee(int id, Employee updateEmployee)
        {
            if (ModelState.IsValid)
            {
                Employee employee = CompanyDatabase.Employees.Where(cond => cond.EmployeeId == id).FirstOrDefault();
                if (employee != null)
                {
                    employee.Email = updateEmployee.Email;
                    employee.Grade = updateEmployee.Grade;
                    employee.Gender = updateEmployee.Gender;
                    employee.Name = updateEmployee.Name;
                    employee.DepartmentId = updateEmployee.DepartmentId;
                    CompanyDatabase.SaveChanges();
                    return Ok("Employee updated successfully");
                }
                else
                {
                    return BadRequest(" No employee found to update ");
                }
            }
            else
            {
                return BadRequest(" Invalid input ");
            }
        }

        /// <summary>
        /// To delete an employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IHttpActionResult DeleteEmployee(int id)
        {
            Employee employee = CompanyDatabase.Employees.Where(cond => cond.EmployeeId == id).FirstOrDefault();
            if (employee != null)
            {
                CompanyDatabase.Employees.Remove(employee);
                CompanyDatabase.SaveChanges();
                return Ok("Employee Deleted successfully");
            }
            else
            {
                return BadRequest("No employee found to delete");
            }
        }
    }
}
