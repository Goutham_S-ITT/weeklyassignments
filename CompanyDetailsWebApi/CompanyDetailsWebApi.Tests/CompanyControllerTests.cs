﻿using System;
using CompanyDetailsWebApi.Controllers;
using NUnit.Framework;
using CompanyDetailsWebApi.Models;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http.Results;

namespace CompanyDetailsWebApi.Tests
{
    [TestFixture]
    public class CompanyControllerTests
    {
        public CompanyDatabaseContext CompanyDatabase = new CompanyDatabaseContext();
        public CompanyController companyController = new CompanyController();

        /// <summary>
        /// Get all employee test
        /// </summary>
        [Test]
        public void GetAllEmployees_ReturnAllEmployees()
        {
            var testEmployees = companyController.GetAllEmployees() as OkNegotiatedContentResult<IEnumerable<EmployeeViewModel>>;
            var databaseEmployees = CompanyDatabase.Employees.ToList();
            Assert.AreEqual(testEmployees.Content.Count(), databaseEmployees.Count);
        }

        /// <summary>
        /// Get an employee test
        /// </summary>
        [Test]
        public void GetEmployee_ReturnEmployee()
        {
            var testEmployees = companyController.GetEmployee(2) as OkNegotiatedContentResult<IEnumerable<EmployeeViewModel>>;
            Assert.IsNotNull(testEmployees);
            Assert.AreEqual(testEmployees.Content.FirstOrDefault().EmployeeId, 2);
        }

        /// <summary>
        /// Create employee test
        /// </summary>
        [Test]
        public void PostEmployee_Employee_ReturnMessage()
        {
            Employee employee = new Employee();
            employee.Grade = "A";
            employee.Gender = "Male";
            employee.DepartmentId = 1;
            employee.Email = "goku@mail.com";
            employee.Name = "Goku";
            var responseMessage = companyController.PostEmployee(employee) as OkNegotiatedContentResult<string>;
            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(responseMessage.Content.ToString(), "Employee added successfully");
        }

        /// <summary>
        /// Update employee test
        /// </summary>
        [Test]
        public void PutEmployee_IdEmployee_ReturnMessage()
        {
            Employee employee = new Employee();
            employee.Grade = "B";
            employee.Gender = "Male";
            employee.DepartmentId = 1;
            employee.Email = "goku@mail.com";
            employee.Name = "Goku";
            var responseMessage = companyController.PutEmployee(1027, employee) as OkNegotiatedContentResult<string>;
            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(responseMessage.Content.ToString(), "Employee updated successfully");
        }

        /// <summary>
        /// Delete employee test
        /// </summary>
        [Test]
        public void DeleteEmployee_Id_ReturnMessage()
        {
            var responseMessage = companyController.DeleteEmployee(1029) as OkNegotiatedContentResult<string>;
            Assert.IsNotNull(responseMessage);
            Assert.AreEqual(responseMessage.Content.ToString(), "Employee Deleted successfully");
        }
    }
}
