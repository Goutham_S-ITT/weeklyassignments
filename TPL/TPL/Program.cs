﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TPL
{
    class Program
    {
        static void Main()
        {
            Parallel.For(0, 10, index =>
          {
              Console.WriteLine("index = " + index);
              Thread.Sleep(1000);
          });
            Console.ReadLine();
        }
    }
}
