﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    class Program
    {
        static string data;
        static string text;
        static byte[] buffer { get; set; }
        static Socket socket;
        static Socket accept;
        static Thread sendThread = new Thread(send);
        static Thread recieveThread = new Thread(recieve);
        static void Main()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1111));
            socket.Listen(1);
            Console.WriteLine("\n Waiting for the client to get connected");
            accept = socket.Accept();
            Console.WriteLine("\n Connected to the client");
            sendThread.Start();
            recieveThread.Start();
            recieveThread.Join();
            //sendThread.Abort();
            socket.Close();
            accept.Close();
        }

        public static void recieve()
        {
            while(true)
            {
                buffer = new byte[accept.ReceiveBufferSize];
                int bytesRead = accept.Receive(buffer);
                byte[] formatted = new byte[bytesRead];
                for (int i = 0; i < bytesRead; i++)
                {
                    formatted[i] = buffer[i];
                }
                data = Encoding.ASCII.GetString(formatted);
                Console.WriteLine("\n >> {0}", data);
            } 
        }

        public static void send()
        {
            while (true)
            {
                //Console.Write("\n Enter text : ");
                text = Console.ReadLine();
                Console.WriteLine("\n << {0}", text);
                byte[] data = Encoding.ASCII.GetBytes(text);
                accept.Send(data);
                //Console.WriteLine("\n Data send successfully \n");
            }
        }
    }
}
