﻿using System.Linq;
using System.Web.Mvc;
using CompanyDetails.Models;

namespace CompanyDetails.Controllers
{
    public class HomeController : Controller
    {
        CompanyDbContext database = new CompanyDbContext();
        UserCredential userData = new UserCredential();

        /// <summary>
        /// To display login page
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// To validate login
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Login")]
        public ActionResult LogIn(UserCredential user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userData = database.UserCredentials.Where(cond => cond.LoginId.Equals(user.LoginId) && cond.Password.Equals(user.Password)).FirstOrDefault();
                    if (userData != null)
                    {
                        return RedirectToAction("Employees", "Company");
                    }
                }
                ModelState.AddModelError("", "Login details provided is incorrect.");
                return View(user);
            }
            catch
            {
                ModelState.AddModelError("", "Exception Occured.");
                return View();
            }
        }
    }
}
