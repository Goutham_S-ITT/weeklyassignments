﻿using CompanyDetails.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace CompanyDetails.Controllers
{
    public class CompanyController : Controller
    {
        CompanyDbContext database = new CompanyDbContext();
        
        /// <summary>
        /// To list all employees
        /// </summary>
        /// <returns></returns>
        public ActionResult Employees()
        {
            return View(database.Employees.ToList());
        }

        /// <summary>
        /// For logout
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            return RedirectToAction("Login", "Home");
        }

        /// <summary>
        /// To display employee create page
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            BindDepartmentId();
            BindDepartmentGrade();
            return View();
        }

        /// <summary>
        /// To create employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Employee employee)
        {
            try
            {
                if (employee.Name != null & employee.Gender != null & employee.Grade != null & employee.DepartmentId > 0 & employee.Email != null)
                {
                    database.Employees.Add(employee);
                    database.SaveChanges();
                    return RedirectToAction("Employees");
                }
                else
                {
                    return View(employee);
                }
            }
            catch (Exception message)
            {
                ModelState.AddModelError("", message.Message);
                return View();
            }
        }

        /// <summary>
        /// To display employee edit page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            BindDepartmentGrade();
            BindDepartmentId();
            Employee employee = database.Employees.Find(id);
            return View(employee);
        }

        /// <summary>
        /// To Edit Employee details
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Employee employee, int id)
        {
            try
            {
                if (employee.Name != null & employee.Gender != null & employee.Grade != null & employee.DepartmentId > 0 & employee.Email != null)
                {
                    employee.EmployeeId = id;
                    database.Entry(employee).State = EntityState.Modified;
                    database.SaveChanges();
                    return RedirectToAction("Employees");
                }
                else
                {
                    return View(employee);
                }
            }
            catch (Exception message)
            {
                ModelState.AddModelError("", message.Message);
                return View();
            }
        }

        /// <summary>
        /// To display employee delete page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            Employee employee = database.Employees.Find(id);
            return View(employee);
        }

        /// <summary>
        /// To delete employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = database.Employees.Find(id);
            database.Employees.Remove(employee);
            database.SaveChanges();
            return RedirectToAction("Employees");
        }

        /// <summary>
        /// To diplay details of employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            Employee employee = database.Employees.Find(id);
            return View(employee);
        }

        /// <summary>
        /// To display employees under department
        /// </summary>
        /// <returns></returns>
        public ActionResult Departments(int id)
        {
            try
            {
                List<Employee> employee = database.Employees.ToList();
                if (id == 1)
                {
                    ViewData["Department"] = "Marketing Dept";
                    employee = employee.FindAll(cond => cond.Department.DepartmentName == "Marketing");
                    return View(employee.ToList());
                }
                if (id == 2)
                {
                    ViewData["Department"] = "Sales Dept";
                    employee = employee.FindAll(cond => cond.Department.DepartmentName == "Sales");
                    return View(employee.ToList());
                }
                if (id == 3)
                {
                    ViewData["Department"] = "HR Dept";
                    employee = employee.FindAll(cond => cond.Department.DepartmentName == "HR");
                    return View(employee.ToList());
                }
            }
            catch
            {
                ModelState.AddModelError("", "Exception Occured.");
                return View();
            }
            return View();
        }

        /// <summary>
        /// To get departmentId data for having drop list
        /// </summary>
        public void BindDepartmentId()
        {
            List<SelectListItem> departmentId = new List<SelectListItem>();
            List<Department> departmentList = database.Departments.ToList();
            foreach (Department item in departmentList)
            {
                departmentId.Add(new SelectListItem { Text = item.DepartmentName, Value = item.DepartmentId.ToString() });
            }
            ViewData["DepartmentId"] = departmentId;
        }

        /// <summary>
        /// To get grade data for having drop list
        /// </summary>
        public void BindDepartmentGrade()
        {
            List<SelectListItem> grade = new List<SelectListItem>();
            List<Salary> salaryList = database.Salaries.ToList();
            foreach (Salary item in salaryList)
            {
                grade.Add(new SelectListItem { Text = item.Grade, Value = item.Grade});
            }
            ViewData["Grade"] = grade;
        }
    }
}