﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Web.Script.Serialization;

namespace Vehicle
{
    public class Vehicle
    {
        public string EngineNumber { get; set; }
        public string VehicleColour { get; set; }
        public string TypeOfFuel { get; set; }

        /// <summary>
        /// For adding new vehicle
        /// </summary>
        public virtual void AddVehicle()
        {
            Console.Write("\n Enter engine number : ");
            EngineNumber = Console.ReadLine();
            Console.Write("\n Enter vehicle colour : ");
            VehicleColour = Console.ReadLine();
            Console.Write("\n Enter type of fuel : ");
            TypeOfFuel = Console.ReadLine();
        }

        /// <summary>
        /// To save data to the database
        /// </summary>
        /// <param name="cars"></param>
        public void SaveToDatabase(List<Car> cars)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["TeslaDatabase"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                foreach (Car car in cars)
                {
                    SqlCommand command = new SqlCommand("Insert into TblCarDetails values(@EngineNumber,@colour,@fuelType,@Airbags)", connection);
                    command.Parameters.AddWithValue("@EngineNumber", car.EngineNumber);
                    command.Parameters.AddWithValue("@colour", car.VehicleColour);
                    command.Parameters.AddWithValue("@fuelType", car.TypeOfFuel);
                    command.Parameters.AddWithValue("@Airbags", car.Airbags);
                    command.ExecuteNonQuery();
                }
            }
            if (cars.Count != 0)
            {
                Console.WriteLine("\n Data saved successfully \n");
            }
            else
            {
                Console.WriteLine("\n No data to save \n");
            }
        }
        
        /// <summary>
        /// To retrieve data from the database 
        /// </summary>
        /// <param name="cars"></param>
        public void RetrieveFromDatabase(List<Car> cars)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["TeslaDatabase"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TblCarDetails;", connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Car newcar = new Car();
                    newcar.EngineNumber = reader["Engine number"].ToString();
                    newcar.VehicleColour = reader["Colour"].ToString();
                    newcar.TypeOfFuel = reader["Type of fuel"].ToString();
                    newcar.Airbags = Convert.ToInt32(reader["Number of Airbags"]);
                    cars.Add(newcar);
                }
            }
            if (cars.Count != 0)
            {
                Console.WriteLine("\n Data retrieved successfully \n");
            }
            else
            {
                Console.WriteLine("\n No data to retrive \n");
            }
        }
    }
}
