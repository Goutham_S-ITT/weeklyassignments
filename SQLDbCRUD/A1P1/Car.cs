﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace Vehicle
{
    public class Car : Vehicle, IOperationOnVehicle
    {
        public int Airbags { get; set; }

        /// <summary>
        /// For adding new vehicle
        /// </summary>
        public override void AddVehicle()
        {
            int data = 0;
            Console.Write("\n Enter number of airbags : ");
            try
            {
                bool result = int.TryParse(Console.ReadLine(), out data);
                if (result)
                {
                    Airbags = data;
                    base.AddVehicle();
                }
                else
                {
                    Console.WriteLine("\n Invalid input please try again \n");
                    AddVehicle();
                }
            }
            catch
            {
                Console.WriteLine("\n Provide input in numbers \n");
            }
            try
            {
                if (EngineNumber != "" & Airbags > 0 & VehicleColour != "" & TypeOfFuel != "")
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["TeslaDatabase"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        SqlCommand command = new SqlCommand("Insert into TblCar values(@EngineNumber,@colour,@fuelType,@Airbags)", connection);
                        command.Parameters.AddWithValue("@EngineNumber", EngineNumber);
                        command.Parameters.AddWithValue("@colour", VehicleColour);
                        command.Parameters.AddWithValue("@fuelType", TypeOfFuel);
                        command.Parameters.AddWithValue("@Airbags", Airbags);
                        connection.Open();
                        command.ExecuteNonQuery();
                        Console.WriteLine("\n Data saved successfully \n");
                    }
                }
                else
                {
                    Console.WriteLine("\n Input provided cannot be empty please try again \n");
                }
            }
            catch
            {
                Console.WriteLine("\n Unable to create data \n");
            }
        }

        /// <summary>
        /// For finding vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void FindVehicle()
        {
            Console.Write("\n Enter engine number : ");
            string EngineNumber = Console.ReadLine();
            string connectionString = ConfigurationManager.ConnectionStrings["TeslaDatabase"].ConnectionString;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand("SELECT * FROM TblCar Where [EngineNumber]=@EngineNumber", connection);
                    command.Parameters.AddWithValue("@EngineNumber", EngineNumber);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        EngineNumber = reader["EngineNumber"].ToString();
                        VehicleColour = reader["Colour"].ToString();
                        TypeOfFuel = reader["TypeOfFuel"].ToString();
                        Airbags = Convert.ToInt32(reader["NumberOfAirbags"]);
                        Console.WriteLine("\n Data retrieved successfully \n");
                        Console.WriteLine("\n Engine number = {0}, Colour = {1} ,Type of fuel = {2}", EngineNumber, VehicleColour, TypeOfFuel);
                        Console.WriteLine(" Number of Airbags = {0}", Airbags);

                    }
                }
                if (EngineNumber == null)
                {
                    Console.WriteLine("\n No data to retrive \n");
                }
            }
            catch
            {
                Console.WriteLine("\n Unable to find data \n");
            }
        }

        /// <summary>
        /// For updating details of vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void UpdateVehicle()
        {
            Console.WriteLine("\n Enter engine number \n");
            EngineNumber = Console.ReadLine();
            Console.WriteLine("\n Select operation to be performed \n\n 1. Update engine number \n\n 2. Update vehicle colour \n\n 3. Update type of fuel \n\n 4. Update number of Airbags \n\n 5. Exit \n");
            Console.Write("\n Enter your choice : ");
            int data;
            string connectionString = ConfigurationManager.ConnectionStrings["TeslaDatabase"].ConnectionString;
            bool result = int.TryParse(Console.ReadLine(), out data);
            if (result)
            {
                try
                {
                    switch (data)
                    {
                        case 1:
                            Console.Write("\n Enter the engine number : ");
                            string currentEngineNumber = Console.ReadLine();
                            using (SqlConnection connection = new SqlConnection(connectionString))
                            {
                                SqlCommand command = new SqlCommand("update TblCar set EngineNumber=@CurrentEngineNumber where EngineNumber=@PreviousEngineNumber", connection);
                                command.Parameters.AddWithValue("@CurrentEngineNumber", currentEngineNumber);
                                command.Parameters.AddWithValue("@PreviousEngineNumber", EngineNumber);
                                connection.Open();
                                command.ExecuteNonQuery();
                                Console.WriteLine("\n Data saved successfully \n");
                            }
                            break;
                        case 2:
                            Console.Write("\n Enter the vehicle colour : ");
                            string currentColour = Console.ReadLine();
                            using (SqlConnection connection = new SqlConnection(connectionString))
                            {
                                SqlCommand command = new SqlCommand("update TblCar set Colour=@CurrentColour where EngineNumber=@CurrentEngineNumber", connection);
                                command.Parameters.AddWithValue("@CurrentColour", currentColour);
                                command.Parameters.AddWithValue("@CurrentEngineNumber", EngineNumber);
                                connection.Open();
                                command.ExecuteNonQuery();
                                Console.WriteLine("\n Data saved successfully \n");
                            }
                            break;
                        case 3:
                            Console.Write("\n Enter type of fuel : ");
                            string currentFuel = Console.ReadLine();
                            using (SqlConnection connection = new SqlConnection(connectionString))
                            {
                                SqlCommand command = new SqlCommand("update TblCar set TypeOfFuel='@currentFuel' where EngineNumber='@CurrentEngineNumber'", connection);
                                command.Parameters.AddWithValue("@currentFuel", currentFuel);
                                command.Parameters.AddWithValue("@CurrentEngineNumber", EngineNumber);
                                connection.Open();
                                command.ExecuteNonQuery();
                                Console.WriteLine("\n Data saved successfully \n");
                            }
                            TypeOfFuel = Console.ReadLine();
                            break;
                        case 4:
                            int currentNumberOfAirbags;
                            Console.Write("\n Enter number of Airbags : ");
                            result = int.TryParse(Console.ReadLine(), out currentNumberOfAirbags);
                            if (result)
                            {
                                using (SqlConnection connection = new SqlConnection(connectionString))
                                {
                                    SqlCommand command = new SqlCommand("update TblCar set NumberOfAirbags='@currentNumberOfAirbags' where EngineNumber='@CurrentEngineNumber';", connection);
                                    command.Parameters.AddWithValue("@currentNumberOfAirbags", currentNumberOfAirbags);
                                    command.Parameters.AddWithValue("@CurrentEngineNumber", EngineNumber);
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                    Console.WriteLine("\n Data saved successfully \n");
                                }
                            }
                            else
                            {
                                Console.WriteLine("\n Invalid input please try again \n");
                            }
                            break;
                        case 5:
                            break;
                        default:
                            Console.WriteLine("\n Invalid input please try again \n");
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("\n Unable to update data \n");
                }
            }
            else
            {
                Console.WriteLine("\n Invalid input please try again \n");
            }
        }

        /// <summary>
        /// For Deleting a vehicle
        /// </summary>
        /// <param name="cars"></param>
        public void DeleteVehicle()
        {
            Console.Write("\n Enter engine number : ");
            string EngineNumber = Console.ReadLine();
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["TeslaDatabase"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand("Delete FROM TblCar Where [EngineNumber]=@EngineNumber", connection);
                    command.Parameters.AddWithValue("@EngineNumber", EngineNumber);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    Console.WriteLine("\n  Data deleted successfully \n");
                }
            }
            catch
            {
                Console.WriteLine("\n Unable to Delete data \n");
            }
        }
    }
}
