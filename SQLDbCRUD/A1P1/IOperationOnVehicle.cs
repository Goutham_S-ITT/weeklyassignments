﻿using System.Collections.Generic;

namespace Vehicle
{
    public interface IOperationOnVehicle
    {
        /// <summary>
        /// For adding new vehicle 
        /// </summary>
        void AddVehicle();

        /// <summary>
        /// For finding vehicle
        /// </summary>
        /// <param name="cars"></param>
        void FindVehicle();

        /// <summary>
        /// For Updating details of vehicle
        /// </summary>
        /// <param name="cars"></param>
        void UpdateVehicle();

        /// <summary>
        /// For deleting a vehicle
        /// </summary>
        /// <param name="cars"></param>
        void DeleteVehicle();
    }
}
