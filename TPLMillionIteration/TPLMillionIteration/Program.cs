﻿using System;
using System.Threading.Tasks;

namespace TPLMillionIteration
{
    class Program
    {
        static void Main()
        {
            Parallel.For(0 , 10000 , i => runMillionIteration());
            Console.Read();
        }
        static void runMillionIteration()
        { 
            for (int index = 0; index < 1000000; index++)
            {
                Console.WriteLine(index);
            }
        }
    }
}
