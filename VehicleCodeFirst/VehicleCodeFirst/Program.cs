﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;

namespace VehicleCodeFirst
{
    class Program
    {
        static int vehicleChoice = 0;
        public List<Car> Cars = new List<Car>();

        /// <summary>
        /// For selecting vehicle
        /// </summary>
        public void SelectionOFVehicle()
        {
            Console.WriteLine("\n\n\t\t\t\tWelcome To Tesla\n");
            Console.WriteLine("\n Select vehicle \n\n 1. Cars \n");
            Console.Write("\n Enter your operationChoice : ");
            try
            {
                bool result = int.TryParse(Console.ReadLine(), out vehicleChoice);
                if (result & vehicleChoice > 0 & vehicleChoice < 2)
                {
                    SelectionOfOperation();
                }
            }
            catch
            {
                Console.WriteLine("\n Provide input as 1 \n");
            }
        }

        /// <summary>
        /// For selecting operation for selected vehicle
        /// </summary>
        public void SelectionOfOperation()
        {
            Car car = new Car();
            Console.WriteLine("\n Select operation to be performed \n\n 1. Add \n\n 2. Find \n\n 3. Update \n\n 4. Delete \n\n 5. List \n\n 6. Insert data to database \n\n 7. Retrieve data from database \n\n 8. Exit \n");
            Console.Write("\n Enter your operationChoice : ");
            try
            {
                int operationChoice;
                bool result = int.TryParse(Console.ReadLine(), out operationChoice);
                if (result & operationChoice > 0 & operationChoice < 9)
                {
                    switch (vehicleChoice)
                    {
                        case 1:
                            switch (operationChoice)
                            {
                                case 1:
                                    car.AddVehicle();
                                    Cars.Add(car);
                                    break;
                                case 2:
                                    car.FindVehicle(Cars);
                                    break;
                                case 3:
                                    car.UpdateVehicle(Cars);
                                    break;
                                case 4:
                                    car.DeleteVehicle(Cars);
                                    break;
                                case 5:
                                    car.ListVehicle(Cars);
                                    break;
                                case 6:
                                    SaveToDatabase(Cars);
                                    break;
                                case 7:
                                    RetrieveFromDatabase(Cars);
                                    break;
                                case 8:
                                    break;
                                default:
                                    Console.WriteLine("\n Provide input from 1 to 6 \n");
                                    SelectionOfOperation();
                                    break;
                            }
                            break;
                        default:
                            Console.WriteLine("\n Provide input as 1 \n");
                            break;
                    }
                }
            }
            catch
            {
                Console.WriteLine("\n Invalid input please try again \n");
            }
        }

        /// <summary>
        /// To save data to the database
        /// </summary>
        /// <param name="cars"></param>
        public void SaveToDatabase(List<Car> cars)
        {

            using (CarDBContext database = new CarDBContext())
            {
                database.Cars.AddRange(cars);
                database.SaveChanges();
            }
            if (cars.Count != 0)
            {
                Console.WriteLine("\n Data saved successfully \n");
            }
            else
            {
                Console.WriteLine("\n No data to save \n");
            }
        }

        /// <summary>
        /// To retrieve data from database
        /// </summary>
        /// <param name="cars"></param>
        public void RetrieveFromDatabase(List<Car> cars)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CarDBContext"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Cars", connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Car newcar = new Car();
                    newcar.Airbags = Convert.ToInt32(reader["Airbags"]);
                    newcar.EngineNumber = reader["EngineNumber"].ToString();
                    newcar.VehicleColour = reader["VehicleColour"].ToString();
                    newcar.TypeOfFuel = reader["TypeOfFuel"].ToString();
                    cars.Add(newcar);
                }
            }
            if (cars.Count != 0)
            {
                Console.WriteLine("\n Data retrieved successfully \n");
            }
            else
            {
                Console.WriteLine("\n No data to retrive \n");
            }
        }

        /// <summary>
        /// To interact with database
        /// </summary>
        public class CarDBContext : DbContext 
        {
            public DbSet<Car> Cars { get; set; } 
        }

        static void Main()
        {
            Program vehicle = new Program();
            while (true)
            {
                vehicle.SelectionOFVehicle();
            }
        }
    }
}

