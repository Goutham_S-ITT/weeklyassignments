﻿using System.Collections.Generic;

namespace VehicleCodeFirst
{
    public interface IOperationOnVehicle
    {
        /// <summary>
        /// For adding new vehicle 
        /// </summary>
        void AddVehicle();

        /// <summary>
        /// For finding vehicle
        /// </summary>
        /// <param name="cars"></param>
        void FindVehicle(List<Car> cars);

        /// <summary>
        /// For Updating details of vehicle
        /// </summary>
        /// <param name="cars"></param>
        void UpdateVehicle(List<Car> cars);

        /// <summary>
        /// For deleting a vehicle
        /// </summary>
        /// <param name="cars"></param>
        void DeleteVehicle(List<Car> cars);

        /// <summary>
        /// For list all vehicle
        /// </summary>
        /// <param name="cars"></param>
        void ListVehicle(List<Car> cars);
    }
}
